/*
{{- with (page.Store.Get "resource-js") -}}
{{-   with (index . "/js/module/friendly.js") -}}
*/
(function(w,d,n){
  'use strict';
  const N = null;
  const T = true;
  var dnt = (typeof n.doNotTrack !== 'undefined') ? n.doNotTrack
          : (typeof w.doNotTrack !== 'undefined') ? w.doNotTrack
          : (typeof n.msDoNotTrack !== 'undefined') ? n.msDoNotTrack
          : N;
  var gpc = n.globalPrivacyControl;
  // keep tri-state null/true/false
  function tri(v) {var i=parseInt(v);return (1===i||'yes'==v||T===v)?T:(0===i||'no'==v||false===v)?false:N;}
  w.dnt = tri(dnt);
  w.gpc = tri(gpc); // might be null if GPC is not supported by UA
  w.noTrack = (N===w.dnt)&&(N===w.gpc)?N:(T===w.dnt||T===w.gpc);
  if(!w.noTrack) {
    w.__fm_t = Date.now();
    var g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=T;
    const tP = w.trustedTypes.createPolicy('tP',{createScriptURL:(u)=>w.encodeURI(u)});
    g.crossOrigin='anonymous';
    g.integrity='{{- .Data.Integrity -}}';
    g.src=tP.createScriptURL('{{- partialCached "resource/cdn" . -}}{{- .RelPermalink -}}');
    s.parentNode.insertBefore(g,s);
  }
  d.documentElement.classList.add(noTrack?'metrics-off':'metrics-on');
})(window,document,navigator);
/*
{{-   end -}}
{{- end -}}
*/

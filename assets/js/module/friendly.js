/*
Friendly metrics script.

Goals:
- small
- no PII gathered
- try to figure out unique visits anyway
- measure pageload time, preload abandon
- send one initial beacon with: referrer, timezone, mobile flag, pageload time (-1 flags abandoned calls)
- measure real page view time as close as possible
- measure maximum scroll
- support multiple data protection friendly providers
- support one-step and two-step mode

Non-Goals:
- old or minor market share browser support (such as IE, Opera Mini)
- no-script support
- circumventing no-track strategies

Strategies used from
https://nicj.net/beaconing-in-practice/
https://docs.simpleanalytics.com/explained/time-on-page
https://docs.simpleanalytics.com/explained/unique-visits
 */
/* eslint-env browser */
/*
{{- with .Site.Params -}}
{{-   $provider := .friendly_metrics.provider -}}
{{-   $config := index .friendly_metrics $provider -}}
{{-   $hostname := "'+w.location.hostname.replace(/^www\\./,'')+'" -}}
 */
'use strict';

import {M as M} from '../config/{{- $provider -}}.json';

const
  w = window,
  d = w.document,
  T = true,
  U = undefined,
  N = Date.now,
  E = w.addEventListener,
  V = w.navigator,
  A = V.userAgent,
  D = d.documentElement,
  b = d.body || {},
  H = 'Height',
  S = 'scroll' + H,
  O = 'offset' + H,
  C = 'client' + H,
  n = '{{- $config.hostname | default $hostname -}}',
  // Map an array of values V to a deep map structure K
  m = (V, K) => V.reduce((r, v, i) => (
    K[i] && K[i].reduce((c, k, j, a) => (
      c[k] = j === a.length - 1 ? v : c[k] || {}, c[k]
    ), r), r
  ), {}),
  // can you find a smaller RFC 4122 compliant fallback?
  u = () => {
    try{return w.crypto.randomUUID();}
    catch(e){return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,(c,r)=>(r=(Math.random()*16)|0,(c<2?r:(r&0x3)|0x8).toString(16)));}
  },
  P = () => {
    // return scroll position
    try {
      return Math.min(
        100,  // sanitize, limit to 100 percent
        Math.round(
          (20 * ((D.scrollTop || 0) + (D[C] || 0))) /  // scroll pos
          Math.max(b[S] || 0, b[O] || 0, D[C] || 0, D[S] || 0, D[O] || 0)  // height
        ) * 5   // lock to 5 percent steps
      );
    } catch (e) {
      return 0;
    }
  },
  B = [
    // script version
    'hmfm-v0.6.1',
    // hostname (configured host)
    n,
    // bot flag
    V.webdriver || w.__nightmare || w.callPhantom ||
      w._phantom || w.phantom || w.__polypane || w._bot || w.Cypress || (w.chrome && w.chrome.headless) ||
      (/(bot|spider|crawl)/i.test(A) && !/(cubot)/i.test(A))
  ]
;

let t = w.__fm_t || N();  // start time - use from critical inline script if available
let L = -1;  // load time
let y = 0;  // offline/hidden start time
let r = 0;  // scroll position
let o = 0;  // position
let f = U;  // finished - tristate

const
  pageview = () => {
    const
      l = w.location,
      p = l.pathname,
      h = l.hostname,
      R = (d.referrer || '')
        .replace(h, n)
        .replace(/^https?:\/\/((m|l|w{2,3}([0-9]+)?)\.)?([^?#]+)(.*)$/, '$4')
        .replace(/^([^/]+)$/, '$1') || U,
      s = w.screen
    ;
    return [
        // referrer
        R,
        // path
        p,
        // URL with host and path
        l.protocol + '//' + n + p,
        // host (original host if different from domain)
        h != n ? h : U,
        // unique flag
        !((['reload', 'back_forward'].indexOf(w.performance.getEntriesByType('navigation')[0].type) > -1) ||
          (R && (R.split('/')[0] == h))),
        // time zone
        (()=> {try {return Intl.DateTimeFormat().resolvedOptions().timeZone;} catch(e) {}})(),
        // page_id
        u(),
        // L is either pageload time or still -1 if abandoned
        L,
        // mobile flag if available
        (V.uaData||{}).mobile,
        // user agent
        A,
        // language
        V.language,
        // screen width
        s.width,
        // screen height
        s.height
    ];
  },
  pagestats = () => [Math.round((N() - t - o) / 1000), Math.max(0, r, P())],
  X = () => {
    if(f) return;  // both possible beacons sent
    var a = 'v', b, c = '{{- .friendly_metrics.pageview | default $config.pageview -}}';
    /* {{- if $config.update -}} */
    if (f === U) {
      f = false;
      B.push(u()); // id and original_id for two-step mode
      b = pageview();
    } else {
      f = T;
      a = 'a'
      c = '{{- .friendly_metrics.update | default $config.update -}}';
      // add page view duration, and max scroll position
      b = pagestats();
      o = 0; // reset offline time
      t = N(); // reset next start time
    }
    b = m([N(), M.t[a], ...B, ...b], [...M.b, ...M[a]]);
    /* {{- else -}} */
    // one-step mode
    f = T;
    B.push(u());
    b = [...pageview(), ...pagestats()];
    b = m([N(), M.t[a], ...B, ...b], [...M.b, ...M.v, ...M.a]);
    /* {{- end -}} */

    b['{{- $config.metadata -}}'] = b.m;
    delete b.m;

    try {
      V.sendBeacon(
        '{{- .friendly_metrics.target | default $config.target -}}' + c,
        JSON.stringify(b)
      );
    } catch (e) {}
  }
;

if ('onpagehide' in w) {
  E('pagehide', X, T);
} else {
  E('unload', X, T);
  E('beforeunload', X, T);
}

E('visibilitychange', () => {
  if (d.hidden) {
    if (!('onpagehide' in w)) X();
    y = N();
  } else o += N() - y;
}, T);

E('load', () => {
  // Calculate load time, reset start time as reference point for events and exit
  const n = N();
  L = n - t;
  t = n;
  /* {{- if $config.update -}} */
  X();
  /* {{- end -}} */
  r = P();
  E('scroll', () => {
    let p = P();
    if (r < p) r = p;
  });
}, T);
/*
{{- end -}}
*/

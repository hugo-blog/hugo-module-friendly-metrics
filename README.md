# Hugo module for friendly metrics

This module is meant to integrate any metrics script in a friendly way.
If the user agent sends a (now obsolete) "do not track" (DNT) or
"global privacy control" (GPC) signal, the metrics script will not be loaded.

# Usage

The module provides four partials

1. `dnt-state.html` that is meant to be but into the `<head>` section of your
HTML code; this loads your metrics script ONLY if no DNT/GPC signal was
received
2. `dnt-badge.html` that shows the state of the DNT/GPC signal and could be
placed in the footer or whrere appropriate; themes might want to override this
3. `no-cookies.html` with a link to the
[EFF where cookies are explained](https://ssd.eff.org/en/glossary/cookies);
please use this ONLY if your site does not set any cookies
4. `no-fingerprints.html` with a link to the
[EFF where user agent fingerprinting is explained](https://ssd.eff.org/en/module/what-fingerprinting);
please use this ONLY if you refrain from using
these techniques in your metrics scripts

# Default integration

This module includes a metrics script that is compatible with two privacy
friendly metrics solutions

* [plausible.io](https://plausible.io/)
* [Simple Analytics](https://www.simpleanalytics.com/de)

It is not easy to choose between those two options because they have very
similar goals and the pricing of the SaaS offering is roughly the same.
But we have some guidelines.

If you want the ability to run your own metrics service, choose plausible.io.
Both the server-side code and the script are free and open source under the AGPL
license. However, plausible.io _does_ collect PII (hashed IP addresses).

The Simple Analytics approach is to avoid sending PII _at all_. Core metrics
such as uniqueness and time spent page on a page are calculated on the client
side.
This makes it possible to completely avoid the processing PII or data that
could be used to fingerprint the user agent. However, the server-side logic of
Simple Analytics is _not_ open source, so you have to rely on their services.
Strictly speaking, this is [Service as a Software Substitute (SaaSS)](https://www.gnu.org/philosophy/who-does-that-server-really-serve.html.en).

The good news here is that the friendly metrics script works much like the
[Simple Analytics Script](https://github.com/simpleanalytics/scripts), which is available under the MIT license,
and can send this data to plausible.io. So, in theory, you could run a (tweaked)
version of plausible.io that does not need to process PII _at all_.
